Title: DebConf Crowdfunding
Slug: debconf-crowdfuding
Date: 2019-05-31 12:30
Author: Daniel Pimentel
Artist: Catarse
Tags: debconf19, debconf, sponsors, crowdfunding
Lang: en
Status: published

# The project

The Debian Brasil Community created this crowdfunding that anyone can make a 
donation above R$ 10,00 for the DebConf19 realization - Debian Project's 
Worldwide Developers Conference.

This will be the 20th edition of the Conference and only the second time it 
will happen in Brazil!

We want to show how the Brazilian Debian community is strong, engaged and 
could be one of the official sponsors of DebConf19. If we hit the goals, the 
Debian Brasil logo will be placed in the sponsorship category equivalent to 
what is collected.

In the sponsorship plan of DebConf19 there are several quotas that your company
can acquire one of them if you prefer. Our initial collection goal in this 
campaign is R$ 9,195.00. Discounting the catharse fee and the cost of 
production of the shirt, gives R$ 7,000, and it is equivalent to a bronze 
quota in the sponsorship plan.

If we do not reach the initial goal, we will only be supporters.

Remarks about the rewards:
- We will send the stickers for everyone in Brazil without additional cost.
- We can send the shirt to anywhere, if the donor pays the shipping cost.


# Extended Goals

If we exceed our initial goals, the next step will be to try to reach R$ 
21,265.00 and thus we will acquire the silver quota (R$ 17,500.00 
discounting the fee).

If we exceed the value of the silver quota, then the objective will be to 
reach the R$ 41,380.00 of the gold quota (R$ 35,000.00 discounting the fee).

And if we exceed this amount, we will try the maximum amount of sponsorship 
that is the R $ 81,610.00 of the platinum quota (R$ 70,000.00 discounting 
the fee).

We believe it is a dream come true and we count on you to be part of it!

# What is DebConf?

DebConf is the Debian Project's Worldwide Developer Conference. The event is 
held annually and it is itinerant, happening in a different city / country 
every year.

DebConf is an event open to everyone, regardless of your level of knowledge 
about Debian. We want to receive from inexperient users who are initiating 
their contact with Debian to Official Project Developers. That is, they are 
all invited!

In addition to a full schedule of technical, social and political lectures, 
DebConf creates an opportunity for developers, contributors and other 
interested people to meet face to face and work together during the event.

DebConf is a totally free event, meaning you do not have to pay for 
registration to attend. The conference is governed entirely by volunteers 
and funded by generous donations from organizations or individuals, including 
travel sponsorship that makes it possible for developers worldwide to 
collaborate in person.

# DebConf19

DebConf19 will be the 20th edition of the Conference and will take place 
from July 21 to 28, 2019, at the UTFPR - Federal Technological University 
of Paraná, Curitiba - Paraná Central Campus.

This will be the second time that Brazil receives DebConf. DebConf4 was 
held in Porto Alegre in 2004.

We are counting on your help in this collective funding campaign whether you 
are a Debian distribution user or not.

## Budget

- Acquisition of the bronze quota of DebConf19: R$ 7,000.00. This amount 
will be used to pay expenses in the event.

## Rewards

- Production: R$ 1,000.00.

## Rate of catarse

- 13% of the total: R$ 1,195.00.
