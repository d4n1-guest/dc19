account assets:cash
account assets:debian-france
account assets:ICTL
account assets:SPI

account expenses:bursaries:bursaries
account expenses:bursaries:diversity
account expenses:bursaries:travel
account expenses:child care
account expenses:content
account expenses:daytrip
account expenses:fees
account expenses:graphic materials:banner
account expenses:graphic materials:paper
account expenses:graphic materials:poster
account expenses:incidentals
account expenses:incidentals:bank
account expenses:incidentals:frontdesk
account expenses:incidentals:misc
account expenses:incidentals:transport
account expenses:insurance
account expenses:local team:computer
account expenses:local team:food
account expenses:local team:transportation
account expenses:party:cheese and wine
account expenses:party:conference dinner:food
account expenses:party:conference dinner:drink
account expenses:party:conference dinner:bus
account expenses:postal
account expenses:press
account expenses:roomboard:accommodation:access point
account expenses:roomboard:accommodation:bedrooms
account expenses:roomboard:accommodation:cleaning
account expenses:roomboard:accommodation:internet link
account expenses:roomboard:accommodation:vegan food
account expenses:roomboard:bar beer
account expenses:roomboard:food:catering
account expenses:roomboard:food:coffee and tea
account expenses:roomboard:food:food
account expenses:sponsors
account expenses:swag:backpack
account expenses:swag:badge paper
account expenses:swag:drink cup
account expenses:swag:havaianas
account expenses:swag:lanyard
account expenses:swag:t-shirt
account expenses:travel costs for invited speaker
account expenses:venue:rent
account expenses:venue:staff
account expenses:video:cable hdmi
account expenses:video:computer rental
account expenses:video:fiber
account expenses:video:general
account expenses:video:insurence
account expenses:video:projector
account expenses:video:sound equipament
account incomes:registration
account incomes:donation
account incomes:roomboard:accommodation
account incomes:roomboard:bar
account incomes:sponsors:bronze
account incomes:sponsors:gold
account incomes:sponsors:platinum
account incomes:sponsors:silver
account incomes:sponsors:supporter

account liabilities:helen
account liabilities:phls
account liabilities:lenharo
